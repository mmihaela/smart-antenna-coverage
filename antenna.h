#ifndef _ANTENNA
#define _ANTENNA

#include <iostream>

using namespace std;

class Antenna {

public:
	int x;
	int y;

	int raza;
	double cost;

	int model;

	Antenna();
	Antenna(int _x, int _y, int _model);

	Antenna& operator=(const Antenna& ref_antenna) {
		x =  ref_antenna.x;
		y = ref_antenna.y;
		raza = ref_antenna.raza;
		cost = ref_antenna.cost;
		model = ref_antenna.model;

		return *this;
	}

	friend ostream& operator<<(ostream& outs, const Antenna& obj) {
		return outs << "x: " << obj.x << "  y: " << obj.y << " C: "
				<< obj.cost << " R: " << obj.raza;
	}
};

#endif
