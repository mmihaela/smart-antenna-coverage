#include "antenna.h"

Antenna::Antenna(){
	x = 0;
	y = 0;
	raza = 0;
	cost = 0;
	model = -1;
}

Antenna::Antenna(int _x, int _y, int _model){
	x = _x;
	y = _y;
	model = _model;
	switch(_model){
		case 0:
			cost = 100;
			raza = 20;
			break;
		case 1:
			cost = 200;
			raza = 35;
			break;
		case 2:
			cost = 150;
			raza = 50;
			break;
		default:
			cost = -1;
			raza = -1;
			cout<<"ERROR: Unknown Antenna Model.\n";
			break;
	}
}
