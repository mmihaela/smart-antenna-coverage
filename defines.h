#ifndef _DEFINES_H
#define _DEFINES_H

#include <math.h>

#define ALL_OK			            0
#define ERROR			            1
#define FILE_NOT_FOUND	            2
#define FILE_WRONG_TYPE	            3
#define ARG_NO_WRONG	            4
#define MALLOC_ERROR	            5

#define NR_INIT_SOLUTIONS(area)     (sqrt(area) / 3)
#define NR_MIN_ANTENE(area)         ((area) / 1024)
#define NR_MAX_ANTENE(area)         (1.5 * NR_MIN_ANTENE(area))


#define ALLOC_MATRIX(matrix, no_rows, no_columns, type)	do {		\
	(matrix) =(type **) malloc((no_rows) * sizeof(type*));			\
	if ((matrix) == NULL)											\
		cout<<"ERROR: could not allocate memory"<<endl;				\
	int i;															\
	for (i = 0; i < (no_rows); ++i)									\
		(matrix)[i] = (type *)calloc((no_columns), sizeof(type));	\
} while(0)


#define FREE_MATRIX(matrix, no_rows) do {\
	int i;								\
	for (i = 0; i < (no_rows); ++i)		\
		free((matrix)[i]);				\
	free((matrix));						\
} while(0)

#endif
