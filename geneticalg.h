/*
 * geneticalg.h
 *
 *  Created on: Nov 3, 2013
 *      Author: croco
 */

#ifndef GENETICALG_H_
#define GENETICALG_H_

#include "solutie.h"
#include <vector>
#include <math.h>

class Genetic_alg {

public:
	Genetic_alg();
	Genetic_alg(vector<Solutie> _solutions);

	vector<Solutie> solutions;

	void evolution();

	friend ostream& operator<<(ostream& outs, const Genetic_alg& obj) {
		outs << "Numar total de solutii: " << obj.solutions.size() << endl;
		for (unsigned int i = 0; i < obj.solutions.size(); i++)
			outs << "\t" << obj.solutions[i] << endl;
		return outs;
	}

	void sort_solutions();

private:
	vector<Solutie> tmp_solution;
	vector<double> history;

	void elite_cromozoms();
	bool evaluate_stop();
	void crossover();
	void crossover_parents(Solutie &sol1, Solutie &sol2);
};

#endif
