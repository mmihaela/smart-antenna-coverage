#ifndef _SOLUTIE
#define _SOLUTIE

#include <iostream>
#include "antenna.h"
#include "map.h"
#include <vector>

using namespace std;

class Solutie {

public:
	Solutie();
	Solutie(Map map);

	vector<Antenna> antene;
	double evaluate();
	void add_antenna(Antenna _ant);
	double acoperire, cost;
	short int **cover_matrix;
	double raport;
	Map map;

	Solutie& operator=(const Solutie& ref_sol) {
		antene = ref_sol.antene;
		acoperire = ref_sol.acoperire;
		cost = ref_sol.cost;
		map = ref_sol.map;
		raport = ref_sol.raport;

		return *this;
	}

	friend ostream& operator<<(ostream& outs, const Solutie& obj) {
		return outs << "Solutie cu " << obj.antene.size()
				<< " antene, acoperire: " << obj.acoperire << ", cost: "
				<< obj.cost << ", raport: " << obj.raport;
	}

	Solutie(const Solutie& ref_sol) {
		antene = ref_sol.antene;
		acoperire = ref_sol.acoperire;
		cost = ref_sol.cost;
		map = ref_sol.map;
		raport = ref_sol.raport;
	}

private:
	int get_height_cover(Antenna ant, int padding, int current_x);
	void get_cover(Antenna ant);
	int get_overlap(Antenna ant1, Antenna ant2);
	bool has_signal(int x1, int y1, int x2, int y2);
	short int ** gen_short_int_matr(int H, int W, int _default);
	void overlap_plus_plus(int poz_a_x, int poz_a_y, int raza,
				short int ** mini_map, short int ** overlap_count);
};

#endif
