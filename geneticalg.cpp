/*
 * geneticalg.cpp
 *
 *  Created on: Nov 3, 2013
 *      Author: croco
 */

#include "geneticalg.h"
#include <algorithm>
#include <stdlib.h>

Genetic_alg::Genetic_alg() {
	tmp_solution = vector<Solutie>();
	history = vector<double>();
}

Genetic_alg::Genetic_alg(vector<Solutie> _solutions) {
	solutions = _solutions;
	tmp_solution = vector<Solutie>();
	history = vector<double>();
}

void Genetic_alg::evolution() {
	while (true) {
		sort_solutions();

		elite_cromozoms();
		cout << "Choosing elite chromosomes...DONE" << endl;

		cout<<"Size of elite chromo: "<<tmp_solution.size()<<endl;

		if (evaluate_stop())
			break;

		crossover();
		cout << "Crossover chromosomes...DONE" << endl;

		tmp_solution.clear();
	}
}

bool Genetic_alg::evaluate_stop() {
	//cout << "start  " << history.size() << endl;
	double medie_current = 0;
	double medie_history = 0;
	double deviation = 0, diff;
	int i;
	int count = 0;

	for (i = 0; i < (int) tmp_solution.size(); i++) {
		//cout << "rap: " << tmp_solution[i].raport << endl;
		medie_current += tmp_solution[i].raport;
	}
	medie_current /= tmp_solution.size();

	if (history.size() < 5) {
		history.push_back(medie_current);
		/*cout << "history vector: ";
		for (i = 0; i < (int) history.size(); i++)
			cout << history[i] << " ";
		cout << endl;*/
		return false;
	}

	/*cout << "history vector: ";
	for (i = 0; i < (int) history.size(); i++)
		cout << history[i] << " ";
	cout << endl;*/

	for (i = (int) history.size() - 1;
			(i >= 0) && (i > ((int) history.size() - 6)); i--) {
		count++;
		medie_history += history[i];
		//cout << history[i] << " ";
	}
	//cout << endl;
	medie_history = medie_history / count;

	//cout << "medie_history: " << medie_history << endl;

	deviation = 0;
	for (i = (int) history.size() - 1;
			(i >= 0) && (i > ((int) history.size() - 6)); i--) {
		diff = medie_history - history[i];
		if (diff < 0)
			diff *= -1;
		deviation += diff;
	}
	//cout << "deviation: " << deviation << endl;

	history.push_back(medie_current);

	if (deviation < 0.01)
		return true;
	return false;
}

bool solution_compare(const Solutie& sol1, const Solutie& sol2) {
	return sol1.raport < sol2.raport;
}

void Genetic_alg::sort_solutions() {
	cout << "Solutions evaluation - Starting..." << endl;
	cout << "Solutions number: " << solutions.size() << endl;

	vector<Solutie>::iterator it;
	int cont = 0;
	for (it = solutions.begin(); it != solutions.end(); ++it) {
		cout << "Solutia " << cont + 1 << " - "
				<< (cont * 100) / solutions.size() << "%" << endl;
		(*it).evaluate();
		cont++;
	}

	cout << "Solution evaluation - DONE" << endl;

	sort(solutions.begin(), solutions.end(), solution_compare);
}

void Genetic_alg::elite_cromozoms() {
	Solutie sol_tmp;
	int sol_selectate = 0;
	int sol_dorite = solutions.size() / 10;
	int contor = 0;
	int bad_sol = 0;
	int init_size = solutions.size();

	while (sol_selectate < sol_dorite && contor < init_size) {
		sol_tmp = solutions[solutions.size() - bad_sol - 1];
		if (sol_tmp.acoperire
				> (0.6 * (sol_tmp.map.height * sol_tmp.map.width))) {
			tmp_solution.push_back(sol_tmp);
			sol_selectate++;
			solutions.erase(solutions.end() - bad_sol - 1);
		} else {
			bad_sol++;
		}
		contor++;
	}
}

void Genetic_alg::crossover() {
	int pos;
	Solutie parent1, parent2;
	while (solutions.size() > 1) {
		pos = rand() % solutions.size();
		parent1 = solutions.at(pos);
		solutions.erase(solutions.begin() + pos);

		pos = rand() % solutions.size();
		parent2 = solutions.at(pos);
		solutions.erase(solutions.begin() + pos);

		crossover_parents(parent1, parent2);

		parent1.acoperire = -1;
		parent2.acoperire = -1;

		tmp_solution.push_back(parent1);
		tmp_solution.push_back(parent2);
	}
	solutions.clear();
	solutions = tmp_solution;
}

void Genetic_alg::crossover_parents(Solutie &sol1, Solutie &sol2) {
	vector<Antenna> antene_temp;
	int pos1 = rand() % sol1.antene.size();
	int pos2 = rand() % sol2.antene.size();
	int i;

	for (i = sol1.antene.size() - 1; i >= pos1; i--) {
		antene_temp.push_back(sol1.antene.back());
		sol1.antene.pop_back();
	}

	for (i = sol2.antene.size() - 1; i >= pos2; i--) {
		sol1.antene.push_back(sol2.antene.back());
		sol2.antene.pop_back();
	}

	while (!antene_temp.empty()) {
		sol2.antene.push_back(antene_temp.back());
		antene_temp.pop_back();
	}

}

