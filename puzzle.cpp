#include "puzzle.h"

Puzzle::Puzzle() {
	cost = 0;
}

Puzzle::Puzzle(Map& map) {
	cout << "Puzzle constructor, generating puzzle\n";
	cost = 0;
	generate_puzzle(map);
	compute_cost(map);
	generate_solutions(map);
}

void Puzzle::generate_puzzle(Map& map) {
	int i, split;
	Area neigh[4];
	// lista de arii neacoperite
	list<Area> arii_proc;

	// Aria totala a hartii
	double total_area = map.width * map.height;
	// Aria partiala ce va fi pusa in lista
	double parent_area, area = 0;

	arii_proc.push_back(Area(0, 0, map.height, map.width));

	// TO DO: De paralelizat
	while (!arii_proc.empty() && total_area != area) {

		//Scot o arie din lista de arii neacoperite
		Area parent = arii_proc.back();
		arii_proc.pop_back();

		//  Elimina arii mici
		//cout << "Parent: " << parent << endl;
		parent_area = parent.get_area();
		if (parent_area < AREA_MIN(total_area) || parent_area <= 1) {
			arii_puzzle.push_back(parent);
			area += parent.get_area();
			continue;
		}

		//Area child - arie generata random.
		Area child = Area();
		child.generate_area(parent.x1, parent.y1, parent.x2, parent.y2,
				total_area);
		//cout << "Child: " << child << endl;

		//Adaug in lista de arii neacoperite cele 0-4 arii
		split = rand() % 2;
		child.get_neighbors(parent, child, neigh, split);
		for (i = 0; i < 4; i++)
			if (neigh[i].get_area())
				arii_proc.push_back(neigh[i]);

		//Adaug aria generata in lista ariilor acoperite.
		arii_puzzle.push_back(child);
		area += child.get_area();
		//cout << "Puzzle area este " << area << endl << endl;

	}
	cout << "Am generat " << arii_puzzle.size() << " subarii\n";
}

bool cmp_by_noise(const Area &a, const Area &b) {
	return a.noise < b.noise;
}

void Puzzle::compute_cost(Map& map) {
	list<Area>::iterator it;

	for (it = arii_puzzle.begin(); it != arii_puzzle.end(); ++it)
		cost += it->get_noise(map);

	arii_puzzle.sort(cmp_by_noise);
	cout << "Final cost " << cost << endl;

	//for (it = arii_puzzle.begin(); it != arii_puzzle.end(); ++it)
	//   cout << "Subarea " << *it << endl;
}

// Genereaza un numar limitat de solutii initiale
void Puzzle::generate_solutions(Map& map) {
    int i, m, local_antene;
    double noise;
    long nr_min_antene = NR_MIN_ANTENE(map.height * map.width);
    long nr_max_antene = NR_MAX_ANTENE(map.height * map.width);
    long nr_solutions = NR_INIT_SOLUTIONS(map.height * map.width);
    long cost_per_ant = 0, antennas_rest = 0;
    list<Area>::iterator it;


    for (i = 0; i < nr_solutions; i++) {
        m = nr_min_antene + rand() % (nr_max_antene - nr_min_antene);
        cost_per_ant = cost / m;
        antennas_rest = m;
        Solutie solutie(map);

        // Pentru fiecare subarie din puzzle genereaza un numar
        // de antene proportional cu noise
        for (it = arii_puzzle.begin(); it != arii_puzzle.end(); ++it) {
            noise = it->noise;
            local_antene = noise / cost_per_ant;
            antennas_rest -= local_antene;

            place_rnd_antennas(*it, solutie, local_antene, map);
        }


        // Placing the rest of antennas one by one based on the subareas noise
        // Will start with the noisiest subarea
        for (it = arii_puzzle.end(); it != arii_puzzle.begin(); --it) {
            if (it == arii_puzzle.end())
                it--;
            if (antennas_rest == 0)
                break;

            noise = it->noise;
            place_rnd_antennas(*it, solutie, 1, map);
            antennas_rest--;
        }

        // Adauga solutia la vectorul de solutii initiale
        init_solutions.push_back(solutie);
    }
}


// Genereaza un numar de local_antene antene plasate random
// in subarea si le adauga in solutie
void Puzzle::place_rnd_antennas(Area subarea, Solutie& solutie,
		int local_antene, Map map) {
	int i, x, y, model;

	for (i = 0; i < local_antene; i++) {
		do {
			x = subarea.x1 + rand() % (subarea.x2 - subarea.x1 + 1);
		} while (x >= map.height);

		do {
			y = subarea.y1 + rand() % (subarea.y2 - subarea.y1 + 1);
		} while (y >= map.width);

		model = rand() % 3;

		Antenna antenna(x, y, model);
		solutie.add_antenna(antenna);
	}
}
