#include <math.h>
#include "solutie.h"
#include "defines.h"

Solutie::Solutie() {
	acoperire = -1;
	cost = -1;
	cover_matrix = NULL;
}

Solutie::Solutie(Map _map) {
	acoperire = -1;
	cost = -1;
	map = _map;
	cover_matrix = NULL;
}

void Solutie::add_antenna(Antenna _ant) {
	acoperire = -1;
	cost = -1;
	antene.push_back(_ant);
}

/**
 * Pentru evaluare se mapeaza fiecare antena intr-o matrice de dimensiunea
 * hartii. Matricea initiala este initializata cu zero, iar pe masura ce sunt
 * adaugate antene , zonele acoperite de sunt incrementate.
 */
double Solutie::evaluate() {
	if (acoperire != -1)
		return acoperire;

	acoperire = 0;
	cost = 0;
	raport = 0;
	unsigned int i;
	int cont = 0;

	ALLOC_MATRIX(cover_matrix, map.height, map.width, short int);

	cout << "\tNumar antene in solutie: " << antene.size() << endl;

	for (i = 0; i < antene.size(); i++) {
		get_cover(antene[i]);
		cost += antene[i].cost;
		cont++;
	}

	FREE_MATRIX(cover_matrix, map.height);
	raport = acoperire / cost;
	return acoperire;
}

short int ** Solutie::gen_short_int_matr(int H, int W, int _default) {
	short int **mini_map;
	int i, j;

	mini_map = (short int**) malloc(H * sizeof(short int*));
	if (mini_map == NULL) {
		return NULL;
	}

	for (i = 0; i < H; i++) {
		mini_map[i] = (short int*) malloc(W * sizeof(short int));
		if (mini_map[i] == NULL)
			return NULL;
	}

	for (i = 0; i < H; i++)
		for (j = 0; j < W; j++)
			mini_map[i][j] = _default;

	return mini_map;
}

void free_mem(short int ** mem, int H) {
	for (int i = 0; i < H; i++)
		free(mem[i]);
	free(mem);
}


int Solutie::get_height_cover(Antenna ant, int padding, int current_x) {
	int j;
	int acoperire = 0;

	for (j = ant.y - ant.raza + padding; j <= ant.y + ant.raza - padding; j++) {
		if (j < 0 || j >= map.width)
			continue;
		if (cover_matrix[current_x][j] != 0)
			continue;
		if (has_signal(ant.x, ant.y, current_x, j))
			cover_matrix[current_x][j]++;
		acoperire += (cover_matrix[current_x][j] == 1) ? 1 : 0;
	}

	return acoperire;
}

// Calculeaza acoperirea unei antene
void Solutie::get_cover(Antenna ant) {
	int i, pad;

	/* portiunea de deasupra antenei de coordonate (x, y) */
	for (i = ant.x, pad = 0; i >= ant.x - ant.raza; i--, pad++) {
		if (i < 0 || i >= map.height)
			continue;
		acoperire += get_height_cover(ant, pad, i);
	}

	/* portiunea de sub antena de coordonate (x, y) */
	for (i = ant.x, pad = 0; i <= ant.x + ant.raza; i++, pad++) {
		if (i < 0 || i >= map.height)
			continue;
		acoperire += get_height_cover(ant, pad, i);
	}

}

int Solutie::get_overlap(Antenna ant1, Antenna ant2) {
	int contor = 0;
	int dif_x = ant1.x - ant2.x;
	int dif_y = ant1.y - ant2.y;

	if (ant1.raza + ant2.raza < sqrt(dif_x * dif_x + dif_y * dif_y))
		return 0;

	int x1 = min(ant1.x - ant1.raza, ant2.x - ant2.raza);
	int y1 = min(ant1.y - ant1.raza, ant2.y - ant2.raza);

	int x2 = max(ant1.x + ant1.raza, ant2.x + ant2.raza);
	int y2 = max(ant1.y + ant1.raza, ant2.y + ant2.raza);

	int H = abs(x2 - x1) + 1;
	int W = abs(y2 - y1) + 1;

	short int ** mini_map = gen_short_int_matr(H, W, -1);
	short int ** overlap_count = gen_short_int_matr(H, W, 0);

	if (mini_map == NULL || overlap_count == NULL) {
		cout << "There is a fucking error !!!" << endl;
		return -1;
	}

	int i, j, lin, col;

	for (i = x1, lin = 0; i <= x2; i++, lin++) {
		if (i >= 0 && i < map.height)
			for (j = y1, col = 0; j <= y2; j++, col++)
				if (j >= 0 && j < map.width)
					mini_map[lin][col] = map.map[i][j];
	}

	overlap_plus_plus(ant1.x - x1 + 1, ant1.y - y1 + 1, ant1.raza, mini_map,
			overlap_count);
	overlap_plus_plus(ant2.x - x1 + 1, ant2.y - y1 + 1, ant2.raza, mini_map,
			overlap_count);

	for (i = 0; i < H; i++)
		for (j = 0; j < W; j++)
			if (overlap_count[i][j] > 1)
				contor++;

	free_mem(mini_map, H);
	free_mem(overlap_count, H);

	return contor;
}

void Solutie::overlap_plus_plus(int poz_a_x, int poz_a_y, int raza,
		short int ** mini_map, short int ** overlap_count) {

	int i, j;

	for (i = poz_a_x - raza; i < poz_a_x + raza; i++)
		for (j = poz_a_y - raza; j < poz_a_y + raza; j++)
			if (has_signal(poz_a_x, poz_a_y, i, j))
				overlap_count[i][j]++;
}

// Verifica daca exista un obstacol intre doua puncte
bool Solutie::has_signal(int x1, int y1, int x2, int y2) {
	if (map.map[x2][y2] > map.map[x1][y1])
		return false;

	double A = y1 - y2;
	double B = x2 - x1;
	double C = (x1 * y2) - (y1 * x2);

	int xMin = min(x1, x2);
	int xMax = max(x1, x2);
	int yMin = min(y1, y2);
	int yMax = max(y1, y2);

	for (int i = xMin; i <= xMax; i++)
		for (int j = yMin; j <= yMax; j++) {
			if (map.map[i][j] > map.map[x1][y1]) {

				double dx[] = { 0.5, 0.5, -0.5, -0.5 };
				double dy[] = { 0.5, -0.5, 0.5, -0.5 };

				for (int k = 0; k < 4; k++) {
					dx[k] += i;
					dy[k] += j;
				}

				double rez[] = { 0, 0, 0, 0 };
				for (int k = 0; k < 4; k++)
					rez[k] = A * dx[k] + B * dy[k] + C;

				int contor = 0;
				for (int k = 0; k < 4; k++) {
					if (rez[k] > 0)
						contor++;
					if (rez[k] < 0)
						contor--;
				}

				if (contor != 4 && contor != -4)
					return false;
			}
		}

	return true;
}
