#ifndef _MAP_DATA
#define _MAP_DATA

#include "defines.h"
#include <string.h>
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;

class Map {
public:
	Map();
	Map(char* _f_image);

	int read_pgm();
	int read_pgm(char* f_image);

	// Copy constructor for Map
	// Does not copy the if_stream field
	Map(const Map& other) {
		map = other.map;
		height = other.height;
		width = other.width;
		maxVal = other.maxVal;
		puzzle_map = other.puzzle_map;
		memcpy(f_image, other.f_image, 256);
	}

	Map& operator=(const Map& ref_map) {
		map = ref_map.map;
		puzzle_map = ref_map.puzzle_map;
		height = ref_map.height;
		width = ref_map.width;
		maxVal = ref_map.maxVal;
		memcpy(f_image, ref_map.f_image, 256);

		return *this;
	}

private:
	int aloc_map();

public:
	int height;
	int width;
	int maxVal;
	unsigned char** map;
	unsigned char** puzzle_map;
	char f_image[256];
	ifstream img_file;

	friend ostream& operator<<(ostream& outs, const Map& obj) {
		return outs << "Harta-> H: " << obj.height << " W: " << obj.width
				<< " MaxVal: " << obj.maxVal;
	}

};

#endif
