#ifndef _PUZZLE_H_
#define _PUZZLE_H_

#include <iostream>
#include <algorithm>
#include <list>
#include "area.h"
#include "map.h"
#include "defines.h"
#include "solutie.h"

using namespace std;

class Puzzle {

public:
	// List of puzzle aries; all areas combined form the main area;
	list<Area> arii_puzzle;
	// Initial solutions
	vector<Solutie> init_solutions;
	double cost;

	Puzzle();
	Puzzle(Map& map);

	friend ostream& operator<<(ostream& outs, const Puzzle& obj) {
		outs << "The solutions vector contains " << obj.init_solutions.size()
				<< " solutions.\n";
		unsigned int i, j;
		for (i = 0; i < obj.init_solutions.size(); i++) {
			outs << "Solution " << i + 1 << ":\n";
			outs << "\tTotal number of elements: "
					<< obj.init_solutions[i].antene.size() << endl;

			for (j = 0; j < obj.init_solutions[i].antene.size(); j++) {
				if (j % 2 == 0)
					outs << endl << "\t\t";

				outs << " A" << j + 1 << "- " << obj.init_solutions[i].antene[j]
						<< " |||\t";
			}
			outs << endl;
		}
		return outs;
	}

private:
	void generate_puzzle(Map& map);
	void compute_cost(Map& map);
	void generate_solutions(Map& map);
	void place_rnd_antennas(Area subarea, Solutie& solutie, int nr, Map map);
};

#endif
