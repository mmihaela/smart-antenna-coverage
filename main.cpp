#include <iostream>
#include <list>

#include "defines.h"
#include "map.h"
#include "area.h"
#include "puzzle.h"
#include "geneticalg.h"

using namespace std;

int tratare_eroare(int error) {

	switch (error) {
	case FILE_NOT_FOUND:
		cout << "Eroare: Fisierul de intrare nu exista." << endl;
		break;

	case MALLOC_ERROR:
		cout << "Eroare: Malloc error." << endl;
		break;

	case FILE_WRONG_TYPE:
		cout << "Eroare: Fisierul de intrare nu are extensia care trebuie."
				<< endl;
		break;

	case ARG_NO_WRONG:
		cout << "Eroare: Nr de argumente insuficient." << endl;
		cout << "./main <heightmap>" << endl;
		break;

	default:
		cout << "Eroare." << endl;
		break;
	}

	return error;
}

int main(int argc, char *argv[]) {

	int ret_val = 0;
	srand(time(NULL));

	// Daca numarul de argumente este gresit
	if (argc != 2)
		return tratare_eroare(ARG_NO_WRONG);

	Map map(argv[1]);

	// Citesc harta *.pgm, verific sa fie totul okey
	ret_val = map.read_pgm();
	if (ret_val != ALL_OK)
		return tratare_eroare(ret_val);

	cout << map << endl;

	// Generare subarii
	Puzzle puzzle = Puzzle(map);

	// TODO: apeleaza algoritmul genetic
	// solutiile initiale sunt in clasa Puzzle, sunt generate deja
	// si sunt in vector init_solutions;
	// tot ce mai trebuie facut este sa le extragi si sa le folosesti

	Genetic_alg genetic = Genetic_alg(puzzle.init_solutions);

	/*genetic.sort_solutions();

	cout << genetic<<endl;*/

	genetic.evolution();
	genetic.sort_solutions();

	cout << genetic<<endl;

	return ALL_OK;
}
