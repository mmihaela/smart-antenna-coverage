#ifndef _AREA
#define _AREA

#include "map.h"

#include <time.h>
#include <stdlib.h>
#include <iostream>

#define AREA_MIN(area)		(double)0.2 *(area)
#define AREA_MAX(area)		(double)0.5 *(area)
#define HORIZONTAL_SPLIT    0
#define VERTICAL_SPLIT      1

using namespace std;

class Area {

public:
	int x1;		// linie colt stanga sus
	int y1;		// coloana colt stanga sus
	int x2;		// linie colt dreapta jos
	int y2;		// coloana colt dreata jos
	double area;	// aria zonei
	double noise;	// valoarea de zgomot a zonei

	Area();
	Area(int _x1, int _x2, int _y1, int _y2);

	void generate_area(int _x1, int _y1, int _x2, int _y2, double total_area);
	double get_area();
	double get_noise(Map& map);
	bool se_suprapune(Area _arie);
	void get_neighbors(Area parent, Area child, Area *neigh, int split);
	bool operator<(Area sol) {
		return noise < sol.noise;
	}

	friend ostream& operator<<(ostream& outs, const Area& obj) {
		return outs << "x1: " << obj.x1 << "  y1: " << obj.y1 << " |  x2: "
				<< obj.x2 << "  y2: " << obj.y2 << " |  area: " << obj.area
				<< " |  noise: " << obj.noise;
	}

private:
	int random_nr(int min, int max);
};

#endif
