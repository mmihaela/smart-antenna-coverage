#include "area.h"

Area::Area() {
	x1 = 0;
	x2 = 0;
	y1 = 0;
	y2 = 0;
	area = 0;
	noise = 0;
}

Area::Area(int _x1, int _y1, int _x2, int _y2) {
	x1 = _x1;
	x2 = _x2;
	y1 = _y1;
	y2 = _y2;
	area = 0;
	area = get_area();
	noise = 0;
}

void Area::generate_area(int _x1, int _y1, int _x2, int _y2,
		double total_area) {
	do {
		x1 = random_nr(_x1, _x2);
		y1 = random_nr(_y1, _y2);

		x2 = random_nr(x1 + 1, _x2);
		y2 = random_nr(y1 + 1, _y2);

		area = (x2 - x1) * (y2 - y1);
	} while (area > AREA_MAX(total_area) || area < AREA_MIN(area));
}

//TODO MODIFICAT
void Area::get_neighbors(Area parent, Area child, Area *neigh, int split) {
	switch (split) {
	case HORIZONTAL_SPLIT:
		neigh[0] = Area(parent.x1, parent.y1, child.x1, parent.y2);
		neigh[1] = Area(child.x1, parent.y1, child.x2, child.y1);
		neigh[2] = Area(child.x2, parent.y1, parent.x2, parent.y2);
		neigh[3] = Area(child.x1, child.y2, child.x2, parent.y2);
		break;

	case VERTICAL_SPLIT:
		neigh[0] = Area(parent.x1, parent.y1, parent.x2, child.y1);
		neigh[1] = Area(parent.x1, child.y1, child.x1, child.y2);
		neigh[2] = Area(parent.x1, child.y2, parent.x2, parent.y2);
		neigh[3] = Area(child.x2, child.y1, parent.x2, child.y2);
		break;

	default:
		cout << "You need to provide a split\n";
	}
}

// TODO MODIFICAT
double Area::get_area() {
	if (area == 0)
		area = (x2 - x1) * (y2 - y1);
	return area;
}

// Calculare cost al unei arii
double Area::get_noise(Map& map) {
	// TO DO: Functie de evaluat o arie
	int i, j, diff = 0, ref = 0, direction = 0;
	long long count = 0;
	unsigned char **a = map.map;

	// Verifica diferenta fata de pixelul din stanga, sus si
	// coltul din stanga sus
	for (i = x1; i < x2; i++) {
		for (j = y1; j < y2; j++) {
			if (i == x1 && j == y1)
				continue;
			diff = 0;
			ref = 0;
			if (i > x1) {
				diff += a[i - 1][j];
				ref++;
			}
			if (j > y1) {
				diff += a[i][j - 1];
				ref++;
			}
			if (i > x1 && j > y1) {
				diff += a[i - 1][j - 1];
				ref++;
			}
			diff = diff / ref;
			if (diff < a[i][j] && direction == 1) {
				direction = 0;
				count++;
			} else if (diff > a[i][j] && direction == 0) {
				direction = 1;
				count++;
			}
		}
	}

	noise = count;

	return noise;
}

int Area::random_nr(int min, int max) {

	if (min == max)
		return min;

	static int random = 0;
	if (!random) {
		srand(time(NULL));
		random = 1;
	}

	return min + (rand() % (max - min));
}

bool Area::se_suprapune(Area _arie) {
	/*if (x1 < _arie.x2 && x2 > _arie.x1 && 
	 y1 < _arie.y2 && y2 > _arie.y1)
	 return true;*/
	if ((x2 < _arie.x1 || x1 > _arie.x2) && (y2 < _arie.y1 || y1 > _arie.y2))
		return false;

	return true;

	//return false;
}
