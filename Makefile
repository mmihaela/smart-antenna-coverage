CC = g++
CFLAGS = -Wall -g
LDFLAGS = -lm

all:	clean antenna

map.o : map.cpp map.h
	${CC} ${CFLAGS} -c map.cpp

main.o : main.cpp
	${CC} ${CFLAGS} -c main.cpp

antenna.o: antenna.cpp antenna.h
	${CC} ${CFLAGS} -c antenna.cpp

solutie.o: solutie.cpp solutie.h
	${CC} ${CFLAGS} -c solutie.cpp

geneticalg.o: geneticalg.cpp geneticalg.h
	${CC} ${CFLAGS} -c geneticalg.cpp ${LDFLAGS}

puzzle.o:puzzle.cpp puzzle.h
	${CC} ${CFLAGS} -c puzzle.cpp

area.o : area.cpp area.h
	${CC} ${CFLAGS} -c area.cpp

antenna : map.o area.o puzzle.o antenna.o solutie.o geneticalg.o main.o
	${CC} ${CFLAGS} main.o map.o area.o puzzle.o antenna.o solutie.o geneticalg.o ${LDFLAGS} -o antenna
	rm -rf *.o *~

clean:
	rm -rf *.o *~ antenna
