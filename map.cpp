#include "map.h"

Map::Map()
{
	height = 0;
	width = 0;
	maxVal = 0;
	map = NULL;
	puzzle_map = NULL;
}

Map::Map(char* _f_image)
{
	height = 0;
	width = 0;
	maxVal = 0;
	map = NULL;
	puzzle_map = NULL;
	strcpy(f_image , _f_image);
}

int Map::read_pgm(char* _f_image){
	strcpy(f_image , _f_image);
	read_pgm();

	return ALL_OK;
}

int Map::read_pgm(){
		
	img_file.open(f_image);
	
	if( !img_file.is_open() ){
		return FILE_NOT_FOUND;
	}
	
	if( strstr(f_image , ".pgm") == NULL){
		img_file.close();
		return FILE_WRONG_TYPE;
	}
		
	string line;
	int info_line = 0;
	int h = 0;
	int lin = 0 , col = 0;
	
	while( getline(img_file , line)){
		if( line[0] == 'P' || line[0] == '#')
			continue;
			
		if(info_line == 0){
			width = atoi( line.substr( 0 , line.find(' ') ).c_str() );
			height = atoi( line.substr( line.find(' ') ).c_str() );
			info_line ++;
			if( aloc_map() != ALL_OK ){
				img_file.close();
				return MALLOC_ERROR;
			}
			continue;
		}
		
		if(info_line == 1){
			maxVal = atoi( line.c_str() );
			info_line ++;			
			continue;
		}
		
		h = atoi( line.c_str() );
				
		map[lin][col] = h;
				
		col++;
		if(col == width){
			col = 0;
			lin++;
		}
	}
	
	img_file.close();
	
	return ALL_OK;
}

int Map::aloc_map(){
	
	map = NULL;
	puzzle_map = NULL;	
	map = (unsigned char**)malloc(height * sizeof(unsigned char*));	
	puzzle_map = (unsigned char**)malloc(height * sizeof(unsigned char*));	
	if(map == NULL || puzzle_map == NULL)
		return MALLOC_ERROR;

	for(int i=0 ; i<height ; i++){
		map[i] = (unsigned char*)malloc(width * sizeof(unsigned char));
		puzzle_map[i] = (unsigned char*)malloc(width * sizeof(unsigned char));
		if(map[i] == NULL || puzzle_map[i] == NULL)
			return MALLOC_ERROR;
	}

	for(int i=0 ; i<height ; i++)
		for(int j=0 ; j<width ; j++){
			map[i][j] = 0;
			puzzle_map[i][j] = 0;
		}
	
	return ALL_OK;
}
